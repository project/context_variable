<?php
/**
 * @file
 * Miscellaneous variable integrations.
 */

/**
 * Implements hook_context_variable_pre_alter().
 */
function context_variable_context_variable_pre_alter($variable, $value) {
  global $theme;

  // Allow Contextual variables to modify current theme settings.
  if ("theme_{$theme}_settings" == $variable) {
    drupal_static_reset('theme_get_setting');
  }
}
