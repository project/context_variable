<?php
/**
 * @file
 * Site Frontpage variable integration.
 */

/**
 * Implements hook_context_variable_VARIABLE_pre_alter().
 */
function context_variable_context_variable_site_frontpage_pre_alter($value) {
  // Unset $_GET['q'] to allow for modifications to the 'site_frontpage' to come
  // into effect if the current request is for the frontpage.
  if ($_GET['q'] == variable_get('site_frontpage', 'node')) {
    unset($_GET['q']);
  }
}

/**
 * Implements hook_context_variable_VARIABLE_post_alter().
 */
function context_variable_context_variable_site_frontpage_post_alter() {
  // Re-initialize the $_GET['q'] if it was unset earlier allow for an alternate
  // 'site_frontpage' to come into effect.
  if (empty($_GET['q'])) {
    drupal_path_initialize();
  }
}
