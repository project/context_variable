Contextual variables 7.x-1.x-dev, xxxx-xx-xx (development release)
--------------------------------------------------------------------------------

- Added grouping of selected variables by module.
- Added token support to values.
- Added ability to act on altered 'site_frontpage' variable.
- Added ability to variable_set() instead of just override.
- Added ability to alter/react before and after a variable is set.
- Fixed issue when FAPI element has no #type attribute.
- Fixed issue when Variables use element validation.
- Fixed issue with AJAX.



Contextual variables 7.x-1.1, 2013-03-24
--------------------------------------------------------------------------------

- Fixed issues when no variables selected.



Contextual variables 7.x-1.0, 2013-03-24
--------------------------------------------------------------------------------

- Initial commit.
